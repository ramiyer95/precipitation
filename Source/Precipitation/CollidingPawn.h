// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "ThePlanetParent.h"
#include "CollidingPawnMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "CollidingPawn.generated.h"

UCLASS()
class PRECIPITATION_API ACollidingPawn : public APawn
{
	GENERATED_BODY()



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	bool isSpeedBurstActive;

	float MovementForward = 0;
	float MovementRight = 0;

	int speedMultiplier = 14;
	int jumpMultiplier = 1800;

	float hyperFuelQty = 300.0f;

	float velocityMultiplier = 100.0f;
		
	UPrimitiveComponent * primitiveMesh;

	USceneComponent * springArmSceneComponenet;

	int score = 0;

	bool isGameOver = false;

	int highestScore = 0;
	int totalMeteorsCount = 0;
	int lastGameScore = 0;


	//can only use jump when cooldown is over
	bool jumpCooldownOver = true;

public:	
	
	ACollidingPawn();

	// Called to bind functionality to input
	virtual void Tick(float DeltaTime) override;

	UPROPERTY()
	TArray<USpringArmComponent*> theSpringArm;

	UFUNCTION(BlueprintCallable)
	void Jump();


	UFUNCTION(BlueprintCallable)
	void SpeedBurstActivate(float AxisValue);

	UFUNCTION(BlueprintCallable)
	void Turn(float AxisValue);

	void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent);

	UFUNCTION()
	void CheckForFatalCollisions(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	
	UFUNCTION(BlueprintCallable)
	void TurnCamera(float AxisValue);


	void DoubleSpeedMultiplier();
	void HalveSpeedMultiplier();

	void DoubleJumpMultiplier();
	void HalveJumpMultiplier();

	void DoubleSize();
	void HalveSize();

	void FillHyperFuel();

	UFUNCTION(BlueprintCallable)
	int GetScore();
	
	UFUNCTION(BlueprintCallable)
	float GetHyperfuelQty();

	UFUNCTION(BlueprintCallable)
	bool GetHasGameEnded();

	UFUNCTION(BlueprintCallable)
	int GetHighestScore();

	UFUNCTION(BlueprintCallable)
	int GetTotalMeteorsCount();

	UFUNCTION(BlueprintCallable)
	void SetHighestScore(int i);

	UFUNCTION(BlueprintCallable)
	void SetTotalMeteorsCollected(int i);

	UFUNCTION(BlueprintCallable)
	bool ReturnJumpCDStatus();

	void ResetJumpCooldown();

};




