// Fill out your copyright notice in the Description page of Project Settings.

#include "Base_Meteor.h"
#include "Runtime/Core/Public/GenericPlatform/GenericPlatformMath.h"

// Sets default values
ABase_Meteor::ABase_Meteor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABase_Meteor::BeginPlay()
{
	Super::BeginPlay();
	
	ApplyTangentialImpulse();

}

// Called every frame
void ABase_Meteor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABase_Meteor::ApplyTangentialImpulse()
{
		
	// Need to apply a tangential velocity to the meteor when it spawns so that it revolves around the planet
	//  before it lands. 

	// Additionally, this tangential velocity must be in a random direction that is perpendicular to the gravitaional 
	//  force of the planet. Not to mention, this velocity should not be hardcoded. Instead, it should be such that
	//  wherever the meteor spawns, the tangential velocity is applied. 

	// To do this, a point must be found on such a plane whose normal is the spawn point's position vector. 
	// Using this point and the position of the spawnpoint, we can find a vector that is penpendicular to the
	//  the gravitational force on the meteor. 

	// First we take random values of x and y (or y,z or x,z such that there is no division by 0)
	// Next we find the value of z (or x or y) such that the resultant point (x,y,z) lies of the plane.
	// Next we find the direction vector by subtracting this vector with the position of the spawn point
	// This direction vector is perpendicular to the gravtitional force on the meteor.
	// The last step is to give a slight nudge to the meteor using the direction vector as the unit vector.


	FVector currentLocation = GetActorLocation();

	float x = 0.0f;

	float y = 0.0f;

	float z = 0.0f;

	if (currentLocation.Z != 0.0f)
	{

		y = FMath::RandRange(-150.0f, 150.0f);

		x = FMath::RandRange(-150.0f, 150.0f);

		z = -(((x - currentLocation.X)*currentLocation.X + (y - currentLocation.Y)*currentLocation.Y) / currentLocation.Z) + currentLocation.Z;
		
	}
	else if (currentLocation.Y != 0.0f)
	{

		z = FMath::RandRange(-150.0f, 150.0f);

		x = FMath::RandRange(-150.0f, 150.0f);

		y = -(((x - currentLocation.X)*currentLocation.X + (z - currentLocation.Z)*currentLocation.Z) / currentLocation.Y) + currentLocation.Y;

	} else if ( currentLocation.X != 0.0f)
	{
		z = FMath::RandRange(-150.0f, 150.0f);

		y = FMath::RandRange(-150.0f, 150.0f);

		x = -(((y - currentLocation.Y)*currentLocation.Y + (z - currentLocation.Z)*currentLocation.Z) / currentLocation.X) + currentLocation.X;
	
	}


	FVector direction = FVector(x, y, z) - currentLocation;

	float dotp = FVector::DotProduct(direction, currentLocation);

	float magnitude = FVector::Distance(direction, FVector(0, 0, 0));

	direction = direction / magnitude;
	
	UPrimitiveComponent * primitiveMesh = Cast<UPrimitiveComponent>(GetRootComponent());

	FVector force = direction * 10000;

	primitiveMesh->AddImpulse(force, NAME_None, true);

}

