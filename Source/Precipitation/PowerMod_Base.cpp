// Fill out your copyright notice in the Description page of Project Settings.

#include "PowerMod_Base.h"


// Sets default values
APowerMod_Base::APowerMod_Base()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APowerMod_Base::BeginPlay()
{
	Super::BeginPlay();
	

	//Can use the following line to get the static mesh component of the current actor. Can use this 
	//  only if there is a single static mesh component

	thisMesh = Cast<UStaticMeshComponent>(AActor::GetComponentByClass(UStaticMeshComponent::StaticClass()));

	//Need to initialize the values of this PowerModifier

	powerModNumber = FMath::RandRange(1, 4);

	thePowerMod = StaticCast<powerModType>( powerModNumber );
	

	UE_LOG(LogTemp, Warning, TEXT("Power Mod Type : %d"), thePowerMod);
	UE_LOG(LogTemp, Warning, TEXT("Power Mod Location : %s"), *GetActorLocation().ToString());

	if (powerModNumber == 1)
	{

		thisMesh->SetStaticMesh(SmallModifierMesh);

	}
	else if (powerModNumber == 2)
	{

		thisMesh->SetStaticMesh(SlowModifierMesh);

	}
	else if (powerModNumber == 3)
	{

		thisMesh->SetStaticMesh(JumpModifierMesh);

	}
	else if (powerModNumber == 4)
	{

		thisMesh->SetStaticMesh(HyperfuelModifierMesh);

	}

}

void APowerMod_Base::Destroyed()
{

	UE_LOG(LogTemp, Warning, TEXT("power mod has been destroyed"));

}

// Called every frame
void APowerMod_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FRotator rotation = FRotator(0.0f, 2.0f, 0.0f);

	FQuat quatRotation = FQuat(rotation);

	AddActorLocalRotation(quatRotation, false, 0, ETeleportType::None);
	
	FRotator rot = GetActorRotation();

}

int APowerMod_Base::GetPowerModType() const
{
	
	return StaticCast<int>(thePowerMod);
}

