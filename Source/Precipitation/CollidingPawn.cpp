// Fill out your copyright notice in the Description page of Project Settings.

#include "CollidingPawn.h"
#include "Base_Meteor.h"
#include "PowerMod_Base.h"
#include "ThePlanetParent.h"
#include "Runtime/Engine/Public/TimerManager.h"


// Sets default values
ACollidingPawn::ACollidingPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACollidingPawn::BeginPlay()
{
	Super::BeginPlay();

	//Caching the value of primitiveMesh since it is used at multiple places
	primitiveMesh = Cast<UPrimitiveComponent>(GetRootComponent());

	primitiveMesh->OnComponentHit.AddDynamic(this, &ACollidingPawn::CheckForFatalCollisions);
	

	//AActor * rootComponentActor = Cast<AActor>(primitiveMesh);

	//rootComponentActor->GetComponents<USpringArmComponent>(theSpringArm);
	
	//springArmSceneComponenet = Cast<USceneComponent>(theSpringArm[0]);

}

// Called every frame
void ACollidingPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
		

	FVector Location = GetActorLocation();
		
	if (isSpeedBurstActive)
	{
		
		if (hyperFuelQty <= 0)
		{

			isSpeedBurstActive = false;

			SetActorLocation(Location + GetActorForwardVector() * speedMultiplier);

		}
		else
		{

			SetActorLocation(Location + GetActorForwardVector() * speedMultiplier * 3);

			hyperFuelQty--;
			
			isSpeedBurstActive = false;

		}

	}
	else
	{

		SetActorLocation(Location + GetActorForwardVector() * speedMultiplier);

	}


}

// Called to bind functionality to input
void ACollidingPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("Turn", this, &ACollidingPawn::Turn);

	InputComponent->BindAction("Jump", IE_Pressed, this, &ACollidingPawn::Jump);

	InputComponent->BindAxis("SpeedBurst", this, &ACollidingPawn::SpeedBurstActivate);
	
	InputComponent->BindAxis("CameraTurn", this, &ACollidingPawn::TurnCamera);

}


void ACollidingPawn::Jump()
{

	if (!jumpCooldownOver)
	{

		return;
			
	}

	FVector direction = GetActorLocation();

	direction.FVector::Normalize(1.0f);

	direction = direction * jumpMultiplier;
		
	primitiveMesh->AddImpulse(direction, NAME_None, true);

	jumpCooldownOver = false;

	FTimerHandle jumpCDReset;

	GetWorldTimerManager().SetTimer(jumpCDReset, this, &ACollidingPawn::ResetJumpCooldown, 1.0f, false, 4.2f);
	
}


void ACollidingPawn::SpeedBurstActivate(float AxisValue)
{
	if (hyperFuelQty > 0.0f && AxisValue >= 0.80f)
	{

		isSpeedBurstActive = true;

	}

}


void ACollidingPawn::Turn(float AxisValue)
{

	FRotator rotation = FRotator(0.0f, AxisValue, 0.0f);

	FQuat quatRotation = FQuat(rotation);

	AddActorLocalRotation(quatRotation, false, 0, ETeleportType::None);

}


void ACollidingPawn::CheckForFatalCollisions(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	
	ABase_Meteor * castedMeteor = Cast<ABase_Meteor>(OtherActor);
	
	APowerMod_Base * powerModGrabbed = Cast<APowerMod_Base>(OtherActor);

	if (castedMeteor != NULL)
	{

		UE_LOG(LogTemp, Warning, TEXT("Fatal Hit"), *castedMeteor->GetName());


		if (castedMeteor->meteorType == 0)
		{

			score++;

			totalMeteorsCount++;

			if (score > highestScore)
			{

				highestScore = score;

			}
			
			OtherActor->Destroy();

		}
		else
		{

			isGameOver = true;

		}
		
	}

	if (powerModGrabbed != NULL)
	{

		FTimerHandle resetSizeup;
		FTimerHandle resetDoublespeed;
		FTimerHandle resetHighjump;
		
		if (powerModGrabbed->GetPowerModType() == 1)
		{

			HalveSize();

			GetWorldTimerManager().SetTimer(resetSizeup, this, &ACollidingPawn::DoubleSize, 1.0f, false, 14.0f);
			
		}


		if (powerModGrabbed->GetPowerModType() == 2)
		{

			HalveSpeedMultiplier();
			
			GetWorldTimerManager().SetTimer(resetDoublespeed, this, &ACollidingPawn::DoubleSpeedMultiplier, 1.0f, false, 14.0f);
			
		}

		if (powerModGrabbed->GetPowerModType() == 3)
		{

			DoubleJumpMultiplier();

			GetWorldTimerManager().SetTimer(resetHighjump, this, &ACollidingPawn::HalveJumpMultiplier, 1.0f, false, 14.0f);

		}

		if (powerModGrabbed->GetPowerModType() == 4)
		{

			FillHyperFuel();

		}


		OtherActor->Destroy();

	}


}

void ACollidingPawn::TurnCamera(float AxisValue)
{
	FRotator rotation = FRotator(0.0f, 0.0f, AxisValue);

	FQuat quatRotation = FQuat(rotation);
	

}

void ACollidingPawn::DoubleSpeedMultiplier()
{

	speedMultiplier = speedMultiplier * 2;

}

void ACollidingPawn::HalveSpeedMultiplier()
{

	speedMultiplier = speedMultiplier / 2;

}


void ACollidingPawn::DoubleJumpMultiplier()
{

	jumpMultiplier = jumpMultiplier * 2;

}


void ACollidingPawn::HalveJumpMultiplier()
{

	jumpMultiplier = jumpMultiplier / 2;

}

void ACollidingPawn::DoubleSize()
{

	FVector currentScale = this->GetActorScale3D();

	currentScale = currentScale * 2;

	this->SetActorScale3D(currentScale);

}

void ACollidingPawn::HalveSize()
{

	FVector currentScale = this->GetActorScale3D();

	currentScale = currentScale / 2;

	this->SetActorScale3D(currentScale);

}

void ACollidingPawn::FillHyperFuel()
{

	hyperFuelQty += 300.0f;

}

int ACollidingPawn::GetScore()
{
	return score;
}

float ACollidingPawn::GetHyperfuelQty()
{

	//beyond 300, we only want very small increments in the UI progress bar
	float returnValue = hyperFuelQty;

	if (hyperFuelQty > 300.0f)
	{

		returnValue = hyperFuelQty - 300.0f;

		returnValue = returnValue / 25.0f;

		returnValue = returnValue + 300.0f;

	}
	
	return returnValue;

}

bool ACollidingPawn::GetHasGameEnded()
{
	return isGameOver;
}

int ACollidingPawn::GetHighestScore()
{
	return highestScore;
}

int ACollidingPawn::GetTotalMeteorsCount()
{
	return totalMeteorsCount;
}

void ACollidingPawn::SetHighestScore(int i)
{
	//to be called from Blueprint
	highestScore = i;
}

void ACollidingPawn::SetTotalMeteorsCollected(int i)
{
	//to be called from Blueprint
	totalMeteorsCount = i;

}

bool ACollidingPawn::ReturnJumpCDStatus()
{
	return jumpCooldownOver;
}

void ACollidingPawn::ResetJumpCooldown()
{	
	jumpCooldownOver = true;

}



