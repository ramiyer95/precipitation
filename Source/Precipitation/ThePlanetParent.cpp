// Fill out your copyright notice in the Description page of Project Settings.

#include "ThePlanetParent.h"
#include "EngineUtils.h"
#include "CollidingPawn.h"
#include "Base_Meteor.h"
#include "PowerMod_Base.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"



// Sets default values
AThePlanetParent::AThePlanetParent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AThePlanetParent::BeginPlay()
{
	Super::BeginPlay();

	
	ActivePawns = GetPawns();

	ActiveMeteors = GetMeteors();
		
	UE_LOG(LogTemp, Warning, TEXT("No. of pawns "));

	UE_LOG(LogTemp, Warning, TEXT("No. of pawns : %d"), ActivePawns.Num());
	
	FTimerHandle SpawnMeteorTimerHandle;
	GetWorldTimerManager().SetTimer(SpawnMeteorTimerHandle, this, &AThePlanetParent::SpawnMeteors, 5.0f, true, 2.0f);


	FTimerHandle SpawnPowerUpTimerHandle;
	GetWorldTimerManager().SetTimer(SpawnPowerUpTimerHandle, this, &AThePlanetParent::SpawnPowerUps, 15.0f, true, 2.0f);


}


// Called every frame
void AThePlanetParent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ApplyFauxGravityToPawns(ActivePawns, DeltaTime);
	
	ApplyFauxGravityToMeteors(ActiveMeteors, DeltaTime);

}

TArray<ACollidingPawn*> AThePlanetParent::GetPawns()
{
	TArray<ACollidingPawn*> pawns;

	for (TActorIterator<ACollidingPawn> Itr(GetWorld()); Itr; ++Itr)
	{ // Access the subclass instance with the * or -> operators. 
		ACollidingPawn *foundPawn = *Itr; 
		 
		pawns.Add(foundPawn);

		UE_LOG(LogTemp, Warning, TEXT("name : %s"), *Itr->GetName());

	}
	
	return pawns;
}

TArray<ABase_Meteor*> AThePlanetParent::GetMeteors()
{
	TArray<ABase_Meteor*> meteors;

	for (TActorIterator<ABase_Meteor> Itr(GetWorld()); Itr; ++Itr)
	{ // Access the subclass instance with the * or -> operators. 
		ABase_Meteor *foundMeteor = *Itr;

		meteors.Add(foundMeteor);

		UE_LOG(LogTemp, Warning, TEXT("name : %s"), *Itr->GetName());

	}

	return meteors;
}


void AThePlanetParent::ApplyFauxGravityToPawns(TArray<ACollidingPawn*> PawnList, float deltaTime)
{

	for (ACollidingPawn* collidingPawnPtr : PawnList)
	{

		UPrimitiveComponent * primitiveMesh = Cast<UPrimitiveComponent>(collidingPawnPtr->GetRootComponent());

		FVector direction = collidingPawnPtr->GetActorLocation() * -1;

		direction.FVector::Normalize(1.0f);
				
		direction = direction * 100000 * deltaTime;
			
		primitiveMesh->AddForce(direction, NAME_None, true);

	}

}


void AThePlanetParent::ApplyFauxGravityToMeteors(TArray<ABase_Meteor*> MeteorList, float deltaTime)
{

	for (ABase_Meteor* collidingMeteorPtr : MeteorList)
	{
		if (collidingMeteorPtr)
		{
			UPrimitiveComponent * primitiveMesh = Cast<UPrimitiveComponent>(collidingMeteorPtr->GetRootComponent());

			FVector direction = collidingMeteorPtr->GetActorLocation() * -1;

			direction.FVector::Normalize(1.0f);

			direction = direction * 300000 * deltaTime;

			if (primitiveMesh)
			{
				primitiveMesh->AddForce(direction, NAME_None, true);
			}

		}
	}

}

void AThePlanetParent::SpawnMeteors()
{


	//Need to determine if the meteor is golden or not. We donot want too many golden meteors.
	// There are 6 meteors spawning every 5 seconds, which means that there are 72 meteors every minute. 
	// Approximately 8 golden meteors a minute seems good. So there needs to be a 11.11% that the meteor is gold.
	
	SpawnPoints = GetRootComponent()->GetAttachChildren();

	for ( int i = 0; i < SpawnPoints.Num(); i++)
	{		
		FVector location = SpawnPoints[i]->GetComponentLocation();
		FRotator rotation = SpawnPoints[i]->GetComponentRotation();

		FActorSpawnParameters spawnInfo;

		spawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

		int j = FMath::RandRange(0.0f, 100.0f);

		if (j > 11.11)
		{

			ABase_Meteor* meteor = GetWorld()->SpawnActor<ABase_Meteor>(redMeteor, location, GetActorRotation(), spawnInfo);

			meteor->meteorType = 1;

			ActiveMeteors.Add(meteor);
		}
		else
		{

			ABase_Meteor* meteor = GetWorld()->SpawnActor<ABase_Meteor>(goldMeteor, location, GetActorRotation(), spawnInfo);

			meteor->meteorType = 0;

			ActiveMeteors.Add(meteor);
		}
		
		

	}
	
}

void AThePlanetParent::SpawnPowerUps()
{
	//Destroy the already existing Power Up

	for (TActorIterator<APowerMod_Base> Itr(GetWorld()); Itr; ++Itr)
	{ // Access the subclass instance with the * or -> operators. 
		APowerMod_Base *foundPowerMod = *Itr;

		UE_LOG(LogTemp, Warning, TEXT("name : %s"), *Itr->GetName());

		foundPowerMod->Destroy();

	}
	

	FVector sp1 = FVector(0.0f, 0.0f, 0.0f);
	FVector sp2 = FVector(0.0f, 0.0f, 0.0f);

	//Choosing a random spawn point for the power up
	int i = FMath::RandRange(1, 2);

	if (i == 1)
	{

		sp1 = FVector(0.0f, 0.0f, 6100.0f);
		sp2 = FVector(0.0f, 0.0f, -6100.0f);
	}
	else if (i == 2)
	{

		sp1 = FVector(0.0f, 6100.0f, 0.0f);
		sp2 = FVector(0.0f, -6100.0f, 0.0f);
	}

	FRotator rot = FRotator(0.0f, 0.0f, 0.0f);

	FActorSpawnParameters spawnInfo;

	spawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;


	APowerMod_Base* powermod = GetWorld()->SpawnActor<APowerMod_Base>(thePowerMod, sp1, GetActorRotation(), spawnInfo);
	APowerMod_Base* powermod1 = GetWorld()->SpawnActor<APowerMod_Base>(thePowerMod, sp2, GetActorRotation(), spawnInfo);

}


