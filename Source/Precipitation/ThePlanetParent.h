// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CollidingPawn.h"
#include "Base_Meteor.h"
#include "PowerMod_Base.h"
#include "ThePlanetParent.generated.h"

UCLASS()
class PRECIPITATION_API AThePlanetParent : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AThePlanetParent();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


private:
	ACollidingPawn * ThePawn = nullptr;

	float gravityCoeff = 0;

	UPROPERTY()
	TArray <ACollidingPawn*> ActivePawns;


	//Really really important to use UPORPERTY for ActiveMeteors array since meteors are prone to .Destroy() and
	// faux gravity function uses references to meteors in every frame. 
	//Without UPROPERTY, the destroyed meteors will leave behind a nasty "Invalid"-named reference. 
	// These invalid references go through nullptr checks because technically they are not nullptrs. 
	// We want pointers to destroyed objects to be nullptrs so that they fail the nullptr checks and the program
	// remains error free. This is exactly what UPROPERTY will do.
	//No UPROPRTY here will crash Unreal without giving errors and it becomes REALLY tough to pinpoint the mistake.
	// (check journal entry on 19-Dec-2018 for more details)

	UPROPERTY()
	TArray <ABase_Meteor*> ActiveMeteors;

	UPROPERTY()
	TArray<USceneComponent*> SpawnPoints;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ABase_Meteor> redMeteor;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ABase_Meteor> goldMeteor;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<APowerMod_Base> thePowerMod;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UFUNCTION() TArray<ACollidingPawn*> GetPawns();
	
	UFUNCTION() TArray<ABase_Meteor*> GetMeteors();

	void ApplyFauxGravityToPawns(TArray<ACollidingPawn*> PawnList, float deltaTime);

	void ApplyFauxGravityToMeteors(TArray<ABase_Meteor*> PawnList, float deltaTime);

	void SpawnMeteors();

	void SpawnPowerUps();

};
