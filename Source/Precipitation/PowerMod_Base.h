// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PowerMod_Base.generated.h"

UCLASS()
class PRECIPITATION_API APowerMod_Base : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APowerMod_Base();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	virtual void Destroyed();

	enum powerModType {SizeDown, Slower, HighJump, HyperFuel};

	powerModType thePowerMod;

public:	


	UPROPERTY(EditAnywhere)
	int powerModNumber;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	int GetPowerModType() const;

	class UStaticMeshComponent* thisMesh;

	UPROPERTY(EditAnywhere)
	class UStaticMesh * SlowModifierMesh;

	UPROPERTY(EditAnywhere)
	class UStaticMesh * SmallModifierMesh;

	UPROPERTY(EditAnywhere)
	class UStaticMesh * JumpModifierMesh;

	UPROPERTY(EditAnywhere)
	class UStaticMesh * HyperfuelModifierMesh;



};
