// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Base_Meteor.generated.h"

UCLASS()
class PRECIPITATION_API ABase_Meteor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABase_Meteor();

	int meteorID;

	// 1 for Red, 0 for Gold
	int meteorType;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void ApplyTangentialImpulse();

	
};
